import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class BingSearchTest {
    @Test
    public void searchInBing(){
        System.setProperty("webdriver.chrome.driver",
                "C:\\Alex\\programming\\Telerik - QA\\Selenium\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();

        webDriver.get("https://www.bing.com/");
        webDriver.findElement(By.id("sb_form_q")).sendKeys("Telerik Academy Alpha");
        webDriver.findElement(By.id("search_icon")).click();
        String telerikLink = webDriver.findElement(By.xpath("//div[@class='b_title']//h2/a")).getText();

        Assert.assertEquals("IT Career Start in 6 Months - Telerik Academy Alpha",telerikLink);
        webDriver.quit();
    }
}
